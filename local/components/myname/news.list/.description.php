<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => "Список новостей",
    "DESCRIPTION" => "Компонент для вывода списка новостей",
);