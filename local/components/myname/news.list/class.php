<?php

namespace Myname\Components\NewsList;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\LoaderException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Application;
use Bitrix\Main\Context;
use Bitrix\Main\Type\DateTime;
use Bitrix\Iblock\ElementTable;

class NewsListComponent extends \CBitrixComponent
{
    protected function getResult(): array
    {
        if (!Loader::includeModule('iblock')) {
            throw new LoaderException('Модуль инфоблоков не установлен');
        }

        $arRes = [];

        $request = Context::getCurrent()->getRequest();
        $getPage = $request->get('page');
        $page = $getPage ?? 1;
        $page = intval($page);

        $limit = 10;
        $offset = ($page - 1) * $limit;

        $arRes['TOTAL_COUNT'] = $this->getTotalCount();
        $arRes['CURRENT_PAGE'] = $page;
        $arRes['PAGE_COUNT'] = ceil($arRes['TOTAL_COUNT'] / $limit);
        $arRes['DELTA'] = 2; // Количество страниц вокруг текущей страницы

        $cacheTime = $this->arParams['CACHE_TIME'];
        $cacheId = 'myname_news_list_'.$offset;
        $cachePath = '/myname_news_list/';


        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cachePath)) {
            $arRes = $cache->getVars();
            $arRes['CACHE'] = 'Y'; // для проверки кеширования
        } elseif ($cache->startDataCache()) {

            $dbRes = ElementTable::getList([
                'select' => ['ID', 'NAME', 'PREVIEW_TEXT'],
                'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'],
                'order' => ['SORT' => 'DESC'],
                'limit' => $limit,
                'offset' => $offset,
            ]);

            while ($arNews = $dbRes->fetch()) {
                $arRes['ITEMS'][] = $arNews;
            }
            $arRes['CACHE'] = 'N'; // для проверки кеширования

            if (empty($arRes)) {
                $cache->abortDataCache();
            } else {
                $cache->endDataCache($arRes);
            }
        }

        return $arRes;
    }

    protected function getTotalCount()
    {
        if (!Loader::includeModule('iblock')) {
            throw new LoaderException('Модуль инфоблоков не установлен');
        }

        return ElementTable::getCount(
            ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'],
            ['ttl' => 3600],
        );

    }

    public function executeComponent()
    {
        try {
            $this->arResult = $this->getResult();
            $this->includeComponentTemplate();
        } catch (\Exception $e) {
            ShowError($e->getMessage());
        }
    }
}