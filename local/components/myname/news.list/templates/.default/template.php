<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */

?>

<div class="news">
    <div class="container">
        <h1><?= $arParams['PAGE_TITLE'] ?></h1>
        <div class="row">
            <?php foreach ($arResult['ITEMS'] as $itemNews): ?>
                <div class="news-item" id="<?=$this->GetEditAreaID($itemNews['ID'])?>">
                    <h3><?= $itemNews['NAME']; ?></h3>
                    <p><?= $itemNews['PREVIEW_TEXT_CUT']?></p>
                    <a class="news-item__btn" href="/news/<?= $itemNews['ID']; ?>/">Подробнее</a>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if ($arResult['PAGE_COUNT'] > 1): ?>
            <div class="pagination">
                <?php if ($arResult['CURRENT_PAGE'] > 1): ?>
                    <a class="pagination__item prev" href="?page=<?= $arResult['CURRENT_PAGE'] - 1 ?>">&laquo;</a>
                <?php endif; ?>


                <?php if ($arResult['CURRENT_PAGE'] > $arResult['DELTA'] + 1): ?>
                    <a class="pagination__item" href="?page=1">1</a>
                    <?php if ($arResult['CURRENT_PAGE'] > $arResult['DELTA'] + 2): ?>
                        <span class="pagination__item">...</span>
                    <?php endif; ?>
                <?php endif; ?>

                <?php for ($i = max(1, $arResult['CURRENT_PAGE'] - $arResult['DELTA']); $i <= min($arResult['PAGE_COUNT'], $arResult['CURRENT_PAGE'] + $arResult['DELTA']); $i++): ?>
                    <?php if ($i == $arResult['CURRENT_PAGE']): ?>
                        <span class="pagination__item active"><?= $i ?></span>
                    <?php else: ?>
                        <a class="pagination__item" href="?page=<?= $i ?>"><?= $i ?></a>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if ($arResult['CURRENT_PAGE'] < $arResult['PAGE_COUNT'] - $arResult['DELTA']): ?>
                    <?php if ($arResult['CURRENT_PAGE'] < $arResult['PAGE_COUNT'] - $arResult['DELTA'] - 1): ?>
                        <span class="pagination__item">...</span>
                    <?php endif; ?>
                    <a class="pagination__item" href="?page=<?= $arResult['PAGE_COUNT'] ?>"><?= $arResult['PAGE_COUNT'] ?></a>
                <?php endif; ?>

                <?php if ($arResult['CURRENT_PAGE'] < $arResult['PAGE_COUNT']): ?>
                    <a class="pagination__item next" href="?page=<?= $arResult['CURRENT_PAGE'] + 1 ?>">&raquo;</a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>