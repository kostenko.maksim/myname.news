<?php
namespace Myname\Components\NewsDetail;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Iblock\ElementTable;

class NewsDetailComponent extends \CBitrixComponent
{
    protected function getNewsDetail($newsId)
    {
        if (!Loader::includeModule('iblock')) {
            throw new LoaderException('Модуль инфоблоков не установлен');
        }

        return ElementTable::getById($newsId)->fetch();
    }

    public function executeComponent()
    {
        try {
            $newsId = $this->arParams['NEWS_ID'];
            $this->arResult = $this->getNewsDetail($newsId);
            $this->includeComponentTemplate();
        } catch (\Exception $e) {
            ShowError($e->getMessage());
        }
    }
}