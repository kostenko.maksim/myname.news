<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */


$arButtons = CIBlock::GetPanelButtons(
    $arResult['IBLOCK_ID'],
    $arResult['ID'],
    0,
    array('SECTION_BUTTONS' => false, 'SESSID' => false)
);

$arResult['EDIT_LINK'] = $arButtons['edit']['edit_element']['ACTION_URL'];
$arResult['DELETE_LINK'] = $arButtons['edit']['delete_element']['ACTION_URL'];

$arResult['EDIT_LINK_TEXT'] = $arButtons['edit']['edit_element']['TEXT'];
$arResult['DELETE_LINK_TEXT'] = $arButtons['edit']['delete_element']['TEXT'];

$this->AddEditAction($arResult['ID'], $arResult['ADD_LINK'], $arResult['ADD_LINK_TEXT']);
$this->AddEditAction($arResult['ID'], $arResult['EDIT_LINK'], $arResult['EDIT_LINK_TEXT']);
$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], $arResult['DELETE_LINK_TEXT'],
    array('CONFIRM' => 'Будет удалена вся информация, связанная с этой записью. Продолжить?'));
