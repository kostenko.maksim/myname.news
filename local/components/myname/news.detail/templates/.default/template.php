<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */

?>

<div class="news-detail" id="<?=$this->GetEditAreaID($arResult['ID'])?>">
    <div class="container">
        <h1><?= $arResult['NAME'] ?></h1>
        <div class="news-text">
            <p><?= $arResult['DETAIL_TEXT'] ?></p>
            <a href="/news/" class="btn-return">Назад</a>
        </div>
    </div>
</div>