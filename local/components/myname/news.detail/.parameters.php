<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$arComponentParameters = array(
    'PARAMETERS' => array(
        'NEWS_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => 'ID новости',
            'TYPE' => 'STRING',
        ),
    ),
);