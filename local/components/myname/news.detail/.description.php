<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => "Детальная страница новости",
    "DESCRIPTION" => "Компонент для вывода детальной страницы новости",
);