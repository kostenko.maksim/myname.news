<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Новости');
?>

<?php
$APPLICATION->IncludeComponent(
	'myname:news.list',
	'',
	Array(
        'CACHE_TIME' => '3600',
		'CACHE_TYPE' => 'A',
		'IBLOCK_ID' => '4',
		'PAGE_TITLE' => 'Новости'
	)
);
?>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>