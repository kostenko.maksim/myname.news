<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Детальная новость');

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$newsId = intval($request->get('ID'));

$APPLICATION->IncludeComponent(
    'myname:news.detail',
    '',
    array(
        'NEWS_ID' => $newsId,
    )
);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');